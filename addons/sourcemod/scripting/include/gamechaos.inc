
// GameChaos's includes for various specific stuff

#if defined _gamechaos_stocks_included
	#endinput
#endif
#define _gamechaos_stocks_included

#define GC_INCLUDES_VERSION 0x01_00_00
#define GC_INCLUDES_VERSION_STRING "1.0.0"

#include <gamechaos/arrays>
#include <gamechaos/client>
#include <gamechaos/debug>
#include <gamechaos/maths>
#include <gamechaos/misc>
#include <gamechaos/strings>
#include <gamechaos/tempents>
#include <gamechaos/tracing>
#include <gamechaos/vectors>